using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnlineBulletController : BaseBulletController
{
    PhotonView photonView;
    // Start is called before the first frame update
    void Start()
    {
        photonView = GetComponent<PhotonView>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (photonView.isMine)
        {
            OnUpdate();
        }
        
    }

    protected override void OnDie()
    {
        PhotonNetwork.Destroy(photonView);
    }
}
