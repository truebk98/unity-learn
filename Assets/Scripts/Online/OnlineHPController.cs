using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnlineHPController : HPController,IPunObservable
{
    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.isWriting)
        {
            stream.SendNext(currentValue);
            stream.SendNext(maxValue);
        }
        else
        {
            currentValue    = (float)stream.ReceiveNext();
            maxValue        = (float)stream.ReceiveNext();
        }
    }
}
