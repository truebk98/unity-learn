using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon;
public class PhotonController : PunBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        DataManager.Instance.LoadData();
        PhotonNetwork.ConnectUsingSettings("1.0");
    }

    public override void OnConnectedToMaster()
    {
        PhotonNetwork.JoinRoom("Game");
    }

    public override void OnJoinedRoom()
    {
        PhotonNetwork.Instantiate("OnlinePlayer",Vector3.zero,Quaternion.identity,0);
    }

    public override void OnPhotonJoinRoomFailed(object[] codeAndMsg)
    {
        Debug.LogError(codeAndMsg[0]);
        PhotonNetwork.CreateRoom("Game");

    }
}
