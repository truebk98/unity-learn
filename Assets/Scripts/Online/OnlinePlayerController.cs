using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnlinePlayerController : BasePlayerController
{
    PhotonView photonView;

    protected override void OnDie()
    {
        PhotonNetwork.Destroy(photonView);
        if (photonView.isMine)
            PhotonNetwork.Disconnect();
    }

    // Start is called before the first frame update
    protected override void Start()
    {
        base.Start();
        photonView = GetComponent<PhotonView>();
    }

    // Update is called once per frame
    void Update()
    {
        if (photonView.isMine)
        {
            OnUpdate();
        }
    }

    public override void OnHit(float damage)
    {
        photonView.RPC("OnHitOnline",PhotonTargets.All,damage);
    }
    [PunRPC]
    public void OnHitOnline(float damage)
    {
        base.OnHit(damage);
    }

    protected override void Shoot()
    {
        GameObject bullet = PhotonNetwork.Instantiate("OnlineBullet", tranShoot.position, tranShoot.rotation, 0);
        bullet.GetComponent<BaseBulletController>().damage = damage;
    }
}
