using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LTAUnityBase.Base.DesignPattern;
public class PlayerController : BasePlayerController
{
    FilterTargetController filterTarget;

    protected override void Awake()
    {
        base.Awake();
        filterTarget = GetComponent<FilterTargetController>();   
        Observer.Instance.AddObserver(TOPICNAME.ENEMY_DIE, OnEnemyDie);
    }

    protected override void OnDie()
    {
        Debug.Log("OnDie");
    }

    // Update is called once per frame
    void Update()
    {
        OnUpdate();
    }

    void OnEnemyDie(object data)
    {
        EnemyController enemy = (EnemyController)data;
        levelController.CurrentValue += enemy.levelController.Level*50;
    }
}

public class Player : SingletonMonoBehaviour<PlayerController>
{

}
