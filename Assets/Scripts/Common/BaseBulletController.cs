using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IHit
{
    void OnHit(float damage);
}


public abstract class BaseBulletController : MonoBehaviour
{
    public float damage;

    public int count = 0;
    // Start is called before the first frame update
    protected void OnUpdate()
    {
        if (count >= 50)
        {
            OnDie();
            return;
        }
        count++;
        RaycastHit2D hit = Physics2D.Raycast(transform.position, transform.up, 0.2f);
        if (hit.transform != null)
        {
            IHit iHit = hit.transform.parent.GetComponent<IHit>();
            if (iHit != null)
            {
                iHit.OnHit(damage);
                OnDie();
                return;
            }

        }
    }


    protected virtual void OnDie()
    {
        Destroy(gameObject);
    }
}
